<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <a href="<?=site_url('admin/user/'.($role==ROLEPSIKOLOG?'mentor':'member'))?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-left"></i>&nbsp;KEMBALI</a>
          <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-save"></i>&nbsp;SIMPAN</button>
          <?php
          if(!empty($data)) {
            ?>
            <input type="hidden" name="Verify" />
            <button type="button" data-verify="true" class="btn btn-sm btn-success"><i class="far fa-user-check"></i>&nbsp;VERIFIKASI</button>
            <?php
          }
          ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Akun & Data Diri</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <?php
                if(empty($role)) {
                  ?>
                  <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control" name="<?=COL_ROLEID?>" required>
                      <?=GetCombobox("SELECT * FROM _roles where RoleID not in (1,5) ORDER BY RoleID", COL_ROLEID, COL_ROLENAME, (!empty($data)?$data[COL_ROLEID]:null))?>
                    </select>
                  </div>
                  <?php
                }
                ?>
                <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Email</label>
                          <input type="text" class="form-control" name="<?=COL_EMAIL?>" placeholder="Email" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" <?=!empty($data)?'readonly':''?> />
                      </div>
                      <div class="col-sm-6">
                        <label>Nickname</label>
                          <input type="text" class="form-control" name="<?=COL_NM_NICKNAME?>" placeholder="Nickname" value="<?=!empty($data)?$data[COL_NM_NICKNAME]:''?>" required />
                      </div>
                    </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label>Nama Depan</label>
                    <input type="text" class="form-control" name="<?=COL_NM_FIRSTNAME?>" placeholder="Nama Depan" value="<?=!empty($data)?$data[COL_NM_FIRSTNAME]:''?>" required />
                  </div>
                  <div class="col-sm-6">
                    <label>Nama Belakang</label>
                    <input type="text" class="form-control" name="<?=COL_NM_LASTNAME?>" placeholder="Nama Belakang" value="<?=!empty($data)?$data[COL_NM_LASTNAME]:''?>" />
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label>Tanggal Lahir</label>
                    <input type="text" class="form-control datepicker" name="<?=COL_DATE_BIRTH?>" placeholder="yyyy-mm-dd" value="<?=!empty($data)?$data[COL_DATE_BIRTH]:''?>" required />
                  </div>
                  <div class="col-sm-6">
                    <label>No. Telp / HP</label>
                    <input type="text" class="form-control" name="<?=COL_NO_PHONE?>" placeholder="No. Telp / HP" value="<?=!empty($data)?$data[COL_NO_PHONE]:''?>" required />
                  </div>
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <div class="row">
                    <div class="col-sm-12">
                      <textarea class="form-control" name="<?=COL_NM_ADDRESS?>" placeholder="Alamat" required><?=!empty($data)?$data[COL_NM_ADDRESS]:''?></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label>Provinsi</label>
                    <select name="<?=COL_NM_PROVINCE?>" class="form-control no-select2" style="width: 100%" required>
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <label>Kabupaten / Kota</label>
                    <select name="<?=COL_NM_CITY?>" class="form-control no-select2" style="width: 100%" required>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-8">
                    <label>Pekerjaan</label>
                    <input type="text" class="form-control" name="<?=COL_NM_OCCUPATION?>" placeholder="Pekerjaan" value="<?=!empty($data)?$data[COL_NM_OCCUPATION]:''?>" required />
                  </div>
                  <?php
                  if($role == ROLEPSIKOLOG) {
                    ?>
                    <div class="col-sm-4">
                      <label>Pengalaman</label>
                      <input type="text" class="form-control uang text-right" name="<?=COL_NUM_EXPERIENCE?>" placeholder="N Tahun" value="<?=!empty($data)?$data[COL_NUM_EXPERIENCE]:''?>" required />
                    </div>
                    <?php
                  }
                  ?>
                </div>
                <div class="form-group row">
                  <div class="col-sm-8">
                    <label>Status Pernikahan</label>
                    <select name="<?=COL_NM_MARITALSTATUS?>" class="form-control" style="width: 100%" required>
                      <option value="BELUM MENIKAH" <?=!empty($data)&&$data[COL_NM_MARITALSTATUS]=='BELUM MENIKAH'?'selected':''?>>BELUM MENIKAH</option>
                      <option value="MENIKAH" <?=!empty($data)&&$data[COL_NM_MARITALSTATUS]=='MENIKAH'?'selected':''?>>MENIKAH</option>
                    </select>
                  </div>
                </div>
                <?php
                if($role == ROLEPSIKOLOG) {
                  $arrEducation = array();
                  if(!empty($data) && !empty($data[COL_NM_EDUCATIONALBACKGROUND])) {
                    $arrEducation = json_decode($data[COL_NM_EDUCATIONALBACKGROUND]);
                  }
                  ?>
                  <div class="form-group">
                    <label>Riwayat Pendidikan</label>
                    <input type="hidden" name="<?=COL_NM_EDUCATIONALBACKGROUND?>" />
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="mt-2 mb-3">
                          <table id="tbl-education" class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Jenjang</th>
                                <th>Tahun Lulus</th>
                                <th>Nama Institusi</th>
                                <th class="text-center">#</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if(!empty($arrEducation)) {
                                foreach($arrEducation as $edu) {
                                  ?>
                                  <tr>
                                    <td><?=$edu->Grade?></td>
                                    <td><?=$edu->TahunLulus?></td>
                                    <td><?=$edu->Institusi?></td>
                                    <td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="<?=$edu->Grade?>" /></td>
                                  </tr>
                                  <?php
                                }
                              }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>
                                  <select name="Grade" class="form-control" style="min-width: 8vw">
                                    <option value="SD">SD</option>
                                    <option value="SMP">SMP</option>
                                    <option value="SMA/SMK">SMA/SMK</option>
                                    <option value="DIPLOMA">DIPLOMA</option>
                                    <option value="SARJANA">SARJANA</option>
                                    <option value="MAGISTER">MAGISTER</option>
                                    <option value="DOKTOR">DOKTOR</option>
                                  </select>
                                </th>
                                <th class="text-center" style="white-space: nowrap">
                                  <input type="number" class="form-control" name="TahunLulus" placeholder="Tahun Lulus" />
                                </th>
                                <th class="text-center" style="white-space: nowrap">
                                  <input type="text" class="form-control" name="Institusi" placeholder="Nama Institusi" />
                                </th>
                                <th class="text-center">
                                  <button type="button" id="btn-add-education" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                                </th>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                ?>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <?php
        if(empty($data)) {
          ?>
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Password</h5>
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="control-label col-sm-4">Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" />
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-4">Konfirmasi Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="ConfirmPassword" placeholder="Konfirmasi Password" />
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
        <?php
        if($role == ROLEPSIKOLOG) {
          ?>
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Pengaturan</h5>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Layanan Konsultasi</label>
                <div class="row">
                  <div class="col-sm-12">
                    <?php
                    $arrType = array();
                    if(!empty($data)) {
                      $rtype = $this->db->where(COL_USERNAME, $data[COL_USERNAME])->get(TBL_USERTYPE)->result_array();
                      foreach($rtype as $s) {
                        $arrType[] = $s[COL_KD_TYPE];
                      }
                    }
                    ?>
                    <select name="KD_Type[]" class="form-control no-select2" style="width: 100%" multiple>
                      <?=GetCombobox("SELECT * from mtype order by NM_Type", COL_KD_TYPE, COL_NM_TYPE, $arrType)?>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label>Jadwal</label>
                <input type="hidden" name="ScheduleFixed" />
                <input type="hidden" name="ScheduleCalendar" />
                <div class="row">
                  <div class="col-sm-12">
                    <div class="mt-2 mb-3">
                      <table id="tbl-schedule-fixed" class="table table-bordered">
                        <thead>
                          <tr class="row-checkbox">
                            <th colspan="3">
                              <div class="form-check">
                                <input id="scheduleFixed" name="ScheduleTypeFixed" class="form-check-input" type="checkbox" <?=!empty($rScheduleFixed)?'checked':''?>>&nbsp;
                                <label class="form-check-label" for="scheduleFixed">TETAP <small>(Jadwal rutin berdasarkan hari dan jam)</small></label>
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th>Hari</th>
                            <th>Jam</th>
                            <th class="text-center">#</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          foreach($rScheduleFixed as $s) {
                            ?>
                            <tr>
                              <td><?=$s[COL_NM_DAY]?></td>
                              <td><?=$s[COL_DATE_SCHEDULETIME_FROM].' - '.$s[COL_DATE_SCHEDULETIME_TO]?></td>
                              <td class="text-center">
                                <button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button>
                                <input type="hidden" name="idx" value="<?=$s[COL_KD_DAY]?>" />
                              </td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>
                              <select name="Day" class="form-control" style="min-width: 10vw">
                                <?=GetCombobox("SELECT * from mdays order by KD_Day", COL_KD_DAY, COL_NM_DAY, null, true, false, '-- Hari --')?>
                              </select>
                            </th>
                            <th class="text-center" style="white-space: nowrap">
                              <input type="text" class="form-control datetimepicker-input mask-time d-inline-block" id="timepickerFrom" name="TimeFrom" data-toggle="datetimepicker" data-target="#timepickerFrom" placeholder="HH:MM" style="width: 6vw" />
                              <span class="p-1">-</span>
                              <input type="text" class="form-control datetimepicker-input mask-time d-inline-block" id="timepickerTo" name="TimeTo" data-toggle="datetimepicker" data-target="#timepickerTo" placeholder="HH:MM" style="width: 6vw" />
                            </th>
                            <th class="text-center">
                              <button type="button" id="btn-add-schedule-fixed" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                            </th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                    <div class="mt-2 mb-3">
                      <table id="tbl-schedule-calendar" class="table table-bordered">
                        <thead>
                          <tr class="row-checkbox">
                            <th colspan="3">
                              <div class="form-check">
                                <input id="scheduleCalendar" name="ScheduleTypeCalendar" class="form-check-input" type="checkbox" <?=!empty($rScheduleCalendar)?'checked':''?>>&nbsp;
                                <label class="form-check-label" for="scheduleCalendar">SESUAI KALENDER <small>(Jadwal diatur sesuai tanggal dan jam tertentu)</small></label>
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <th>Tanggal</th>
                            <th>Jam</th>
                            <th class="text-center">#</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          foreach($rScheduleCalendar as $s) {
                            ?>
                            <tr>
                              <td><?=$s[COL_DATE_SCHEDULEDATE]?></td>
                              <td><?=$s[COL_DATE_SCHEDULETIME_FROM].' - '.$s[COL_DATE_SCHEDULETIME_TO]?></td>
                              <td class="text-center">
                                <button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button>
                                <input type="hidden" name="idx" value="<?=$s[COL_DATE_SCHEDULEDATE]?>" />
                              </td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>
                              <input type="text" class="form-control calendar" name="Day" placeholder="yyyy-mm-dd" />
                            </th>
                            <th class="text-center" style="white-space: nowrap">
                              <input type="text" class="form-control datetimepicker-input mask-time d-inline-block" id="timepickerCalendarFrom" name="TimeFrom" data-toggle="datetimepicker" data-target="#timepickerCalendarFrom" placeholder="HH:MM" style="width: 6vw" />
                              <span class="p-1">-</span>
                              <input type="text" class="form-control datetimepicker-input mask-time d-inline-block" id="timepickerCalendarTo" name="TimeTo" data-toggle="datetimepicker" data-target="#timepickerCalendarTo" placeholder="HH:MM" style="width: 6vw" />
                            </th>
                            <th class="text-center">
                              <button type="button" id="btn-add-schedule-calendar" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                            </th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<script>
$(document).ready(function() {
  $.get('https://wilayah-api.herokuapp.com/provinsi', function(data) {
    var dataProvinsi = $.map(data, function(n,i){
      return {id: n.name, text: n.name, selected: n.name=='<?=(!empty($data)?$data[COL_NM_PROVINCE]:"--")?>'}
    });

    $('select[name=<?=COL_NM_PROVINCE?>]').select2({
      width: 'resolve',
      theme: 'bootstrap4',
      placeholder: 'Pilih Provinsi',
      data: dataProvinsi
    }).trigger('change');
  });

  $('select[name=<?=COL_NM_PROVINCE?>]').change(function() {
    var dis = $(this);
    var url = 'https://wilayah-api.herokuapp.com/provinsi?name='+dis.val();

    $.get(url, function(data) {
      var provId = 0;
      if(data) {
        provId = data[0].id;
      }

      $.get('https://wilayah-api.herokuapp.com/kabupaten?province_id='+provId, function(kot) {
        var dataKota = $.map(kot, function(n,i){
          return {id: n.name, text: n.name, selected: n.name=='<?=(!empty($data)?$data[COL_NM_CITY]:'--')?>'}
        });

        $('select[name=<?=COL_NM_CITY?>]').empty();
        $('select[name=<?=COL_NM_CITY?>]').select2({
          width: 'resolve',
          theme: 'bootstrap4',
          placeholder: 'Pilih Kabupaten / Kota',
          data: dataKota
        }).trigger('change');
      });
    });
  });

  $('select[name^=KD_Type]').select2({
    width: 'resolve',
    theme: 'bootstrap4',
    placeholder: 'Layanan Konsultasi'
  });
  $('.calendar').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minDate: moment().format('YYYY-MM-DD'),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('button[data-verify=true]').click(function() {
    $('[name=Verify]').val(1);
    $('#form-main').submit();
  });
  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      if(!confirm('Apakah anda yakin?')) {
        return false;
      }

      var btnSubmit = $('button[type=submit], button[data-verify=true]', $(form));
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('[name=ScheduleFixed]').change(function() {
    writeSchedule('tbl-schedule-fixed', 'ScheduleFixed');
  }).val(encodeURIComponent('<?=(!empty($data)?$data['ScheduleFixed']:'')?>')).trigger('change');
  $('[name=ScheduleCalendar]').change(function() {
    writeSchedule('tbl-schedule-calendar', 'ScheduleCalendar');
  }).val(encodeURIComponent('<?=(!empty($data)?$data['ScheduleCalendar']:'')?>')).trigger('change');
  $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').change(function() {
    writeEducation('tbl-education', '<?=COL_NM_EDUCATIONALBACKGROUND?>');
  }).val(encodeURIComponent('<?=(!empty($data)?$data[COL_NM_EDUCATIONALBACKGROUND]:'')?>')).trigger('change');

  $('#btn-add-schedule-fixed', $('#tbl-schedule-fixed')).click(function() {
    var dis = $(this);
    var arr = $('[name=ScheduleFixed]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var day = $('[name=Day]', row).val();
    var timeFr = $('[name=TimeFrom]', row).val();
    var timeTo = $('[name=TimeTo]', row).val();
    if(day && timeFr && timeTo) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Day == day;
      });
      if(exist.length == 0) {
        arr.push({'Day': day, 'DayText': $('[name=Day] option:selected', row).html(), 'TimeFrom':timeFr, 'TimeTo':timeTo});
        $('[name=ScheduleFixed]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi jadwal dengan benar.');
    }
  });

  $('#btn-add-schedule-calendar', $('#tbl-schedule-calendar')).click(function() {
    var dis = $(this);
    var arr = $('[name=ScheduleCalendar]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var day = $('[name=Day]', row).val();
    var timeFr = $('[name=TimeFrom]', row).val();
    var timeTo = $('[name=TimeTo]', row).val();
    if(day && timeFr && timeTo) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Day == day;
      });
      if(exist.length == 0) {
        arr.push({'Day': day, 'DayText': day, 'TimeFrom':timeFr, 'TimeTo':timeTo});
        $('[name=ScheduleCalendar]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi jadwal dengan benar.');
    }
  });

  $('#btn-add-education', $('#tbl-education')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var grade = $('[name=Grade]', row).val();
    var tahun = $('[name=TahunLulus]', row).val();
    var institusi = $('[name=Institusi]', row).val();
    if(grade && tahun && institusi) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Grade == grade;
      });
      if(exist.length == 0) {
        arr.push({'Grade': grade, 'TahunLulus':tahun, 'Institusi':institusi});
        $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi pendidikan dengan benar.');
    }
  });

  $('[name^=ScheduleType]').change(function() {
    //var el = $('[name=ScheduleType]:checked');
    var tbl = $(this).closest('table');
    if($(this).is(':checked')) {
      $('tr:not(.row-checkbox)', tbl).removeClass('d-none');
      $('tr.row-checkbox', tbl).removeClass('row-checkbox').addClass('bg-secondary disabled row-checkbox');
    } else {
      $('tr:not(.row-checkbox)', tbl).addClass('d-none');
      $('tr.row-checkbox', tbl).removeClass('bg-secondary disabled');
    }
  }).trigger('change');
});

function writeSchedule(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      arr = arr.sort(function (a, b) {
        return a['Day'].localeCompare(b['Day']);
      });
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].DayText+'</td>';
        html += '<td>'+arr[i].TimeFrom+' - '+arr[i].TimeTo+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Day+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.Day != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

function writeEducation(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].Grade+'</td>';
        html += '<td>'+arr[i].TahunLulus+'</td>';
        html += '<td>'+arr[i].Institusi+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Grade+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.Grade != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
