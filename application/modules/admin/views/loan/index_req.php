<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title">
              <a href="<?=site_url('admin/loan/add/'.$status)?>" class="btn btn-sm btn-outline-success btn-popup-data"><i class="fas fa-plus"></i>&nbsp;TAMBAH PERMOHONAN</a>
            </h5>
            <div class="card-tools">
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body p-0">
            <form method="post" action="#">
              <table id="list-data" class="table table-bordered mb-0" style="white-space: nowrap;">
                <thead class="font-weight-bold">
                  <tr>
                    <td class="text-center" style="width: 10px">#</td>
                    <td>No. Pinjaman</td>
                    <td>Anggota</td>
                    <td>Jlh. Pinjaman</td>
                    <td>Bunga (%)</td>
                    <td>Tenor (bulan)</td>
                    <td>Tanggal</td>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-loan" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Pinjaman</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var modalLoan = $('#modal-loan');

  var dtLoan = $('#list-data').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/loan/index-load/'.$status)?>",
      "type": 'POST',
    },
    "scrollY" : '30vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row pl-3 pr-3 pt-2'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row pl-3 pr-3 pb-2'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs": [{"targets":[3,4,5], "className":'text-right'}, {"targets":[6], "className":'text-center'}],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": true}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
            }
          }, "json").done(function() {
            dtLoan.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-popup-data', $(row)).click(function() {
        var url = $(this).attr('href');
        $('.modal-body', modalLoan).load(url, function() {
          modalLoan.modal('show');
          $("select", modalLoan).select2({ width: 'resolve', theme: 'bootstrap4' });
          $(".uang", modalLoan).number(true, 0, '.', ',');
          $(".money", modalLoan).number(true, 2, '.', ',');
          $('form', modalLoan).validate({
            submitHandler: function(form) {
              var btnSubmit = $('button[type=submit]', $(form));
              var txtSubmit = btnSubmit[0].innerHTML;
              btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
              $(form).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success: function(res) {
                  if(res.error != 0) {
                    toastr.error(res.error);
                  } else {
                    toastr.success('Berhasil');
                    dtLoan.DataTable().ajax.reload();
                    modalLoan.modal('hide');
                  }
                },
                error: function() {
                  toastr.error('SERVER ERROR');
                },
                complete: function() {
                  btnSubmit.html(txtSubmit);
                }
              });
              return false;
            }
          });
        });
        return false;
      });
    }
  });

  $('.modal').on('hidden.bs.modal', function (event) {
      $('form', $(this)).val('').trigger('change');
  });

  $('.btn-refresh-data').click(function() {
    dtLoan.DataTable().ajax.reload();
  });

  $('.btn-popup-data').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', modalLoan).load(url, function() {
      modalLoan.modal('show');
      $("select", modalLoan).select2({ width: 'resolve', theme: 'bootstrap4' });
      $(".uang", modalLoan).number(true, 0, '.', ',');
      $(".money", modalLoan).number(true, 2, '.', ',');
      $('form', modalLoan).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          var txtSubmit = btnSubmit[0].innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success('Berhasil');
                dtLoan.DataTable().ajax.reload();
                modalLoan.modal('hide');
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
            }
          });
          return false;
        }
      });
    });
    return false;
  });
});
</script>
