<?php
$jlhPinjaman = $data[COL_LOANAMOUNT];
$bungaPersen = $data[COL_LOANINTEREST];
$term = $data[COL_LOANTERM];

$amt = $jlhPinjaman / $term;
$bunga = ($bungaPersen/100)*$jlhPinjaman;
?>
<?=form_open(current_url(), array('class'=>'form-horizontal'))?>
<div class="row">
  <div class="col-sm-12">
    <div class="form-group">
      <label>No. Pinjaman</label>
      <input type="text" name="<?=COL_IDLOAN?>" class="form-control" value="<?="LN-".str_pad($data[COL_IDLOAN], 5, "0", STR_PAD_LEFT)?>" disabled />
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Cicilan Ke</label>
        <input type="text" name="CicilanKe" class="form-control" value="<?=($cicilanKe+1)?>" disabled />
      </div>
      <div class="col-sm-6">
        <label>Tanggal</label>
        <input type="text" name="<?=COL_DATEPAID?>" class="form-control datepicker" />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Cicilan Pokok</label>
        <input type="text" name="<?=COL_AMOUNT?>" class="form-control uang text-right" value="<?=$amt?>" readonly />
      </div>
      <div class="col-sm-6">
        <label>Bunga</label>
        <input type="text" name="<?=COL_INTEREST?>" class="form-control uang text-right" value="<?=$bunga?>" readonly />
      </div>
    </div>
    <div class="form-group">
      <label>Total</label>
      <input type="text" name="Total" class="form-control form-control-lg uang text-right font-weight-bold" value="<?=$amt+$bunga?>" readonly />
    </div>
    <div class="form-group">
      <label>Catatan</label>
      <textarea rows="3" class="form-control" name="<?=COL_REMARKS?>"></textarea>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <button type="submit" class="btn btn-block btn-outline-primary"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
  </div>
</div>
<?=form_close()?>
