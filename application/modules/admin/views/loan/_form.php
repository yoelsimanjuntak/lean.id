<?php
$rolemember = ROLEMEMBER;
?>
<?=form_open(current_url(),array('role'=>'form','id'=>'form-member-edit','class'=>'form-horizontal'))?>
<input type="hidden" name="<?=COL_LOANSTATUS?>" value="<?=!empty($stat)?$stat:''?>" />
<div class="row">
  <div class="col-sm-12">
    <div class="form-group">
      <label>Anggota</label>
      <select name="<?=COL_USERNAME?>" class="form-control" style="width: 100%" required <?=!empty($readonly)?'disabled':''?>>
        <?=GetCombobox("select * from _userinformation left join _users u on u.UserName = _userinformation.UserName where u.RoleID = $rolemember", COL_USERNAME, array(COL_NM_IDENTITYNO, COL_NM_FULLNAME), (!empty($data)?$data[COL_USERNAME]:null))?>
      </select>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Bunga (%)</label>
        <input type="text" class="form-control money text-right" name="<?=COL_LOANINTEREST?>" value="<?=!empty($data)?$data[COL_LOANINTEREST]:''?>" required <?=!empty($readonly)?'disabled':''?> />
      </div>
      <div class="col-sm-6">
        <label>Jumlah</label>
        <input type="text" class="form-control uang text-right" name="<?=COL_LOANAMOUNT?>" value="<?=!empty($data)?$data[COL_LOANAMOUNT]:''?>" required <?=!empty($readonly)?'disabled':''?> />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Tenor (bln)</label>
        <input type="text" class="form-control uang text-right" name="<?=COL_LOANTERM?>" value="<?=!empty($data)?$data[COL_LOANTERM]:''?>" required <?=!empty($readonly)?'disabled':''?> />
      </div>
      <div class="col-sm-6">
        <label>Cicilan / Bln</label>
        <input type="text" class="form-control uang text-right" name="NumCicilan" readonly <?=!empty($readonly)?'disabled':''?> />
      </div>
    </div>
    <!--<div class="form-group">
      <label>Status</label>
      <select name="<?=COL_LOANSTATUS?>" class="form-control" style="width: 100%" required>
        <option value="PERMOHONAN" <?=!empty($data)&&$data[COL_LOANSTATUS]=="PERMOHONAN"?"selected":""?>>DIAJUKAN</option>
        <option value="AKTIF" <?=!empty($data)&&$data[COL_LOANSTATUS]=="AKTIF"?"selected":""?>>DISETUJUI</option>
        <option value="DITOLAK" <?=!empty($data)&&$data[COL_LOANSTATUS]=="DITOLAK"?"selected":""?>>DITOLAK</option>
      </select>
    </div>-->
    <div class="form-group">
      <label>Catatan</label>
      <textarea rows="3" class="form-control" name="<?=COL_REMARKS?>" <?=!empty($readonly)?'disabled':''?>><?=!empty($data)?$data[COL_REMARKS]:''?></textarea>
    </div>
  </div>
</div>
<?php
if(empty($readonly)) {
  ?>
  <div class="row">
    <div class="col-sm-12">
      <button type="submit" class="btn btn-block btn-outline-primary"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
    </div>
  </div>
  <?php
}
?>
<?=form_close()?>
<script>
$(document).ready(function() {
  $('[name=<?=COL_LOANINTEREST?>], [name=<?=COL_LOANAMOUNT?>], [name=<?=COL_LOANTERM?>]').change(function() {
    var amt = $('[name=<?=COL_LOANAMOUNT?>]').val();
    var interest = $('[name=<?=COL_LOANINTEREST?>]').val();
    var term = $('[name=<?=COL_LOANTERM?>]').val();

    var cicilan = 0;
    if(toNum(amt)>0 && toNum(interest)>0 && toNum(term)>0) {
      cicilan = ((toNum(interest)/100)*toNum(amt))+(toNum(amt)/toNum(term));
    }
    $('[name=NumCicilan]').val(cicilan).trigger('change');
  });

  var amt = $('[name=<?=COL_LOANAMOUNT?>]').val();
  var interest = $('[name=<?=COL_LOANINTEREST?>]').val();
  var term = $('[name=<?=COL_LOANTERM?>]').val();

  var cicilan = 0;
  if(toNum(amt)>0 && toNum(interest)>0 && toNum(term)>0) {
    cicilan = ((toNum(interest)/100)*toNum(amt))+(toNum(amt)/toNum(term));
  }
  $('[name=NumCicilan]').val(cicilan).trigger('change');
});
</script>
