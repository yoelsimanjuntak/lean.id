<?php
$cmember = $this->db
->where(COL_ROLEID, ROLEMEMBER)
->get(TBL__USERS)
->num_rows();
$rloan_request = $this->db
->where(COL_LOANSTATUS, 'PERMOHONAN')
->get(TBL_TLOAN)
->num_rows();
$rloan_aktif = $this->db
->where(COL_LOANSTATUS, 'AKTIF')
->get(TBL_TLOAN)
->num_rows();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4">
        <div class="info-box">
          <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-users"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Anggota</span>
            <span class="info-box-number"><?=number_format($cmember)?></span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="info-box">
          <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cash-register"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Permohonan Pinjaman</span>
            <span class="info-box-number"><?=number_format($rloan_request)?></span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="info-box">
          <span class="info-box-icon bg-success elevation-1"><i class="fas fa-cash-register"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Pinjaman Aktif</span>
            <span class="info-box-number"><?=number_format($rloan_aktif)?></span>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Anggota</h5>
            <div class="card-tools">
              <button type="button" class="btn btn-tool btn-refresh-member"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form method="post" action="#">
              <table id="list-member" class="table table-bordered" style="white-space: nowrap;">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>No. Identitas</th>
                    <th>No. Telepon</th>
                    <th>Akun</th>
                    <th>Tgl. Bergabung</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-member" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Anggota</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var modalMember = $('#modal-member');

  var dtmember = $('#list-member').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/dashboard/member-load')?>",
      "type": 'POST',
    },
    "scrollY" : '30vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "buttons": [
      {
        "text": '<i class="far fa-plus"></i>&nbsp;TAMBAH',
        "className": 'btn btn-sm btn-primary',
        "action": function (e, dt, node, config) {
          //modalMemberAdd.modal('show');
          var href = '<?=site_url('admin/dashboard/member-add')?>';
          $('.modal-body', modalMember).load(href, function() {
            modalMember.modal('show');
            $('form', modalMember).validate({
              submitHandler: function(form) {
                var btnSubmit = $('button[type=submit]', $(form));
                var txtSubmit = btnSubmit[0].innerHTML;
                btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
                $(form).ajaxSubmit({
                  dataType: 'json',
                  type : 'post',
                  success: function(res) {
                    if(res.error != 0) {
                      toastr.error(res.error);
                    } else {
                      toastr.success('Berhasil');
                      dtmember.DataTable().ajax.reload();
                      modalMember.modal('hide');
                    }
                  },
                  error: function() {
                    toastr.error('SERVER ERROR');
                  },
                  complete: function() {
                    btnSubmit.html(txtSubmit);
                  }
                });
                return false;
              }
            });
          });
        },
        "init": function(api, node, config) {
          $(node).removeClass('btn-default');
        }
      }
    ],
    "order": [],
    //"columnDefs": [{"targets":[0,1,6,7], "className":'text-center'}],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
      {"orderable": true},
      {"orderable": true}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
            }
          }, "json").done(function() {
            dtmember.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-popup-action', $(row)).click(function() {
        var url = $(this).attr('href');
        $('.modal-body', modalMember).load(url, function() {
          modalMember.modal('show');
          $('form', modalMember).validate({
            submitHandler: function(form) {
              var btnSubmit = $('button[type=submit]', $(form));
              var txtSubmit = btnSubmit[0].innerHTML;
              btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
              $(form).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success: function(res) {
                  if(res.error != 0) {
                    toastr.error(res.error);
                  } else {
                    toastr.success('Berhasil');
                    dtmember.DataTable().ajax.reload();
                    modalMember.modal('hide');
                  }
                },
                error: function() {
                  toastr.error('SERVER ERROR');
                },
                complete: function() {
                  btnSubmit.html(txtSubmit);
                }
              });
              return false;
            }
          });
        });
        return false;
      });
    }
  });

  $('.modal').on('hidden.bs.modal', function (event) {
      $('form', $(this)).val('').trigger('change');
  });

  $('.btn-refresh-member').click(function() {
    dtmember.DataTable().ajax.reload();
  });
});
</script>
