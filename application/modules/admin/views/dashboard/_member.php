<?=form_open(current_url(),array('role'=>'form','id'=>'form-member-edit','class'=>'form-horizontal'))?>
<div class="row">
  <div class="col-sm-12">
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Nama Lengkap</label>
        <input type="text" class="form-control" name="<?=COL_NM_FULLNAME?>" value="<?=!empty($data)?$data[COL_NM_FULLNAME]:''?>" />
      </div>
      <div class="col-sm-6">
        <label>No. Identitas</label>
        <input type="text" class="form-control" name="<?=COL_NM_IDENTITYNO?>" value="<?=!empty($data)?$data[COL_NM_IDENTITYNO]:''?>" />
      </div>
    </div>

    <div class="form-group row">
      <div class="col-sm-6">
        <label>Jenis Kelamin</label>
        <select name="<?=COL_NM_GENDER?>" class="form-control" style="width: 100%">
          <option value="LAKI-LAKI" <?=!empty($data)&&$data[COL_NM_GENDER]=='LAKI-LAKI'?'selected':''?>>LAKI-LAKI</option>
          <option value="PEREMPUAN" <?=!empty($data)&&$data[COL_NM_GENDER]=='PEREMPUAN'?'selected':''?>>PEREMPUAN</option>
        </select>
      </div>
      <div class="col-sm-6">
        <label>No. Telepon / HP</label>
        <input type="text" class="form-control" name="<?=COL_NM_PHONENO?>" value="<?=!empty($data)?$data[COL_NM_PHONENO]:''?>" />
      </div>
    </div>

    <?php
    if(empty($data)) {
      ?>
      <div class="form-group row">
        <div class="col-sm-6">
          <label>Email</label>
          <input type="email" class="form-control" name="<?=COL_NM_EMAIL?>" />
        </div>
        <div class="col-sm-6">
          <label>Password</label>
          <input type="password" class="form-control" name="<?=COL_PASSWORD?>" />
        </div>
      </div>
      <?php
    }
    ?>

    <div class="form-group row">
      <div class="col-sm-12">
        <label>Alamat</label>
        <textarea class="form-control" name="<?=COL_NM_ADDRESS?>"><?=!empty($data)?$data[COL_NM_ADDRESS]:''?></textarea>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <button type="submit" class="btn btn-block btn-outline-primary"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
  </div>
</div>
<?=form_close()?>
