<?php
class Loan extends MY_Controller {

  public function index($status='') {
    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }

    $data['title'] = "Pinjaman [".$status."]";
    $data['status'] = $status;
    if($status=='PERMOHONAN') {
      $this->template->load('main', 'admin/loan/index_req', $data);
    } else {
      $this->template->load('main', 'admin/loan/index', $data);
    }
  }

  public function index_load($status) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_IDLOAN,COL_NM_FULLNAME,COL_CREATEDON);
    $cols = array(COL_IDLOAN,COL_NM_FULLNAME,COL_CREATEDON);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' u','u.'.COL_USERNAME." = ".TBL_TLOAN.".".COL_USERNAME,"inner")
    ->where(TBL_TLOAN.'.'.COL_LOANSTATUS, $status)
    ->get(TBL_TLOAN);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          if($item == COL_NM_FULLNAME) {
            $this->db->like(TBL__USERINFORMATION.'.'.COL_NM_FULLNAME, $_POST['search']['value']);
            $this->db->or_like(TBL__USERINFORMATION.'.'.COL_NM_IDENTITYNO, $_POST['search']['value']);
          } else if($item == COL_IDLOAN) {
            $this->db->like("CONCAT('LN-', LPAD(IdLoan, 5, 0))", $_POST['search']['value']);
          } else {
            $this->db->like($item, $_POST['search']['value']);
          }
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }
    $q = $this->db
    //->select("*, CONCAT(u.Nm_IdentityNo, ' - ', u.Nm_FullName) as Member")
    ->select("*, (select count(*) from tloandetail det where det.IdLoan = tloan.IdLoan) as JlhCicilan, (select sum(det.Amount) from tloandetail det where det.IdLoan = tloan.IdLoan) as TotalCicilan, , (select sum(det.Interest) from tloandetail det where det.IdLoan = tloan.IdLoan) as TotalBunga")
    ->join(TBL__USERINFORMATION.' u','u.'.COL_USERNAME." = ".TBL_TLOAN.".".COL_USERNAME,"inner")
    ->where(TBL_TLOAN.'.'.COL_LOANSTATUS, $status)
    ->get_compiled_select(TBL_TLOAN, false);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      if($status == 'PERMOHONAN') {
        $data[] = array(
          '<a href="'.site_url('admin/loan/changestat/DITOLAK/'.$r[COL_IDLOAN]).'" class="btn btn-xs btn-outline-danger btn-action" title="Tolak"><i class="fas fa-times"></i></a>&nbsp;'.
          '<a href="'.site_url('admin/loan/changestat/AKTIF/'.$r[COL_IDLOAN]).'" class="btn btn-xs btn-outline-primary btn-action" title="Setuju"><i class="fas fa-check"></i></a>&nbsp;'.
          '<a href="'.site_url('admin/loan/view/'.$r[COL_IDLOAN]).'" class="btn btn-xs btn-outline-info btn-popup-data" title="Lihat"><i class="fas fa-eye"></i></a>&nbsp;'.
          '<a href="'.site_url('admin/loan/edit/'.$r[COL_IDLOAN]).'" class="btn btn-xs btn-outline-success btn-popup-data" title="Ubah"><i class="fas fa-pencil"></i></a>',
          "LN-".str_pad($r[COL_IDLOAN], 5, "0", STR_PAD_LEFT),
          $r[COL_NM_FULLNAME],
          number_format($r[COL_LOANAMOUNT], 0),
          number_format($r[COL_LOANINTEREST], 2),
          number_format($r[COL_LOANTERM], 0),
          date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
        );
      } else {
        $btn = array(
          '<a href="'.site_url('admin/loan/view/'.$r[COL_IDLOAN]).'" class="btn btn-xs btn-outline-info btn-popup-data" title="Lihat"><i class="fas fa-eye"></i></a>'
        );
        if($status == 'AKTIF' || $status == 'LUNAS') {
          $btn[] = '<a href="'.site_url('admin/loan/history/'.$r[COL_IDLOAN]).'" data-table-href="'.site_url('admin/loan/history-load/'.$r[COL_IDLOAN]).'" class="btn btn-xs btn-outline-primary btn-popup-data" title="Riwayat Pembayaran"><i class="fas fa-clock"></i></a>';
        }
        if($status == 'AKTIF') {
          $btn[] = '<a href="'.site_url('admin/loan/pay/'.$r[COL_IDLOAN]).'" class="btn btn-xs btn-outline-success btn-popup-data" title="Tambah Cicilan"><i class="fas fa-cash-register"></i></a>';
        }

        $data[] = array(
          implode("&nbsp;", $btn),
          "LN-".str_pad($r[COL_IDLOAN], 5, "0", STR_PAD_LEFT),
          $r[COL_NM_FULLNAME],
          number_format($r[COL_LOANAMOUNT], 0),
          number_format($r[COL_LOANINTEREST], 2),
          number_format($r[COL_LOANTERM], 0),
          number_format($r["JlhCicilan"], 0),
          number_format($r["TotalCicilan"], 0),
          number_format($r["TotalBunga"], 0)
        );
      }

    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add($status='') {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_USERNAME => $this->input->post(COL_USERNAME),
        COL_LOANSTATUS => $this->input->post(COL_LOANSTATUS),
        COL_LOANAMOUNT => !empty($this->input->post(COL_LOANAMOUNT))?toNum($this->input->post(COL_LOANAMOUNT)):0,
        COL_LOANINTEREST => !empty($this->input->post(COL_LOANINTEREST))?toNum($this->input->post(COL_LOANINTEREST)):0,
        COL_LOANTERM => !empty($this->input->post(COL_LOANTERM))?toNum($this->input->post(COL_LOANTERM)):0,
        COL_REMARKS => $this->input->post(COL_REMARKS),
        COL_CREATEDON => date('Y-m-d H:i:s'),
        COL_CREATEDBY => $ruser[COL_USERNAME]
      );

      $res = $this->db->insert(TBL_TLOAN, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        return;
      }

      ShowJsonSuccess('OK');
      return;
    } else {
      $data['stat'] = $status;
      $this->load->view('admin/loan/_form', $data);
    }
  }

  public function edit($id) {
    if(!empty($_POST)) {
      $dat = array(
        COL_USERNAME => $this->input->post(COL_USERNAME),
        COL_LOANAMOUNT => !empty($this->input->post(COL_LOANAMOUNT))?toNum($this->input->post(COL_LOANAMOUNT)):0,
        COL_LOANINTEREST => !empty($this->input->post(COL_LOANINTEREST))?toNum($this->input->post(COL_LOANINTEREST)):0,
        COL_LOANTERM => !empty($this->input->post(COL_LOANTERM))?toNum($this->input->post(COL_LOANTERM)):0,
        COL_REMARKS => $this->input->post(COL_REMARKS)
      );

      $res = $this->db->where(COL_IDLOAN, $id)->update(TBL_TLOAN, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        return;
      }

      ShowJsonSuccess('OK');
      return;
    } else {
      $data['data'] = $rdata = $this->db->where(COL_IDLOAN, $id)->get(TBL_TLOAN)->row_array();
      if(empty($rdata)) {
        echo 'Data tidak valid.';
        return;
      } else if($rdata[COL_LOANSTATUS] == 'AKTIF' || $rdata[COL_LOANSTATUS] == 'LUNAS') {
        echo 'Data tidak dapat diubah dikarenakan status telah berubah.';
        return;
      }
      $this->load->view('admin/loan/_form', $data);
    }
  }

  public function view($id) {
    $data['readonly'] = true;
    $data['data'] = $rdata = $this->db->where(COL_IDLOAN, $id)->get(TBL_TLOAN)->row_array();
    if(empty($rdata)) {
      echo 'Data tidak valid.';
      return;
    }
    $this->load->view('admin/loan/_form', $data);
  }

  public function changestat($stat, $id) {
    $res = $this->db->where(COL_IDLOAN, $id)->update(TBL_TLOAN, array(COL_LOANSTATUS=>$stat));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('OK');
    return;
  }

  public function pay($id) {
    $ruser = GetLoggedUser();
    $data['data'] = $rdata = $this->db->where(COL_IDLOAN, $id)->get(TBL_TLOAN)->row_array();
    if(empty($rdata)) {
      echo 'Data tidak valid.';
      return;
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDLOAN => $id,
        COL_DATEPAID => $this->input->post(COL_DATEPAID),
        COL_AMOUNT => !empty($this->input->post(COL_AMOUNT))?toNum($this->input->post(COL_AMOUNT)):0,
        COL_INTEREST => !empty($this->input->post(COL_INTEREST))?toNum($this->input->post(COL_INTEREST)):0,
        COL_REMARKS => $this->input->post(COL_REMARKS),
        COL_CREATEDON => date('Y-m-d H:i:s'),
        COL_CREATEDBY => $ruser[COL_USERNAME]
      );

      $rdet = $this->db
      ->select("sum(Amount) as Payment")
      ->where(COL_IDLOAN, $id)
      ->get(TBL_TLOANDETAIL)
      ->row_array();

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TLOANDETAIL, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if($rdet["Payment"] + $dat[COL_AMOUNT] >= $rdata[COL_LOANAMOUNT]) {
          $res = $this->db->where(COL_IDLOAN, $id)->update(TBL_TLOAN, array(COL_LOANSTATUS=>'LUNAS'));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    } else {
      $data['cicilanKe'] = $rdetail = $this->db->where(COL_IDLOAN, $id)->get(TBL_TLOANDETAIL)->num_rows();
      $this->load->view('admin/loan/_pay', $data);
    }
  }

  public function history($id) {
    $data['data'] = $rdata = $this->db
    ->where(COL_IDLOAN, $id)
    ->get(TBL_TLOAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak valid.';
      return;
    }

    $this->load->view('admin/loan/_history', $data);
  }

  public function history_load($id) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $orderdef = array(COL_DATEPAID=>'desc');
    $cols = array(COL_DATEPAID,COL_REMARKS);

    $queryAll = $this->db
    ->where(COL_IDLOAN, $id)
    ->order_by(COL_DATEPAID, 'desc')
    ->get(TBL_TLOANDETAIL);

    $queryNoLimit = $this->db
    ->where(COL_IDLOAN, $id)
    ->order_by(COL_DATEPAID, 'desc')
    ->get(TBL_TLOANDETAIL);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    $query = $this->db
    ->where(COL_IDLOAN, $id)
    ->order_by(COL_DATEPAID, 'desc')
    ->limit($rowperpage, $start)
    ->get(TBL_TLOANDETAIL);
    $data = [];

    foreach($query->result_array() as $r) {
      $data[] = array(
        date('d-m-Y', strtotime($r[COL_DATEPAID])),
        number_format($r[COL_AMOUNT]),
        number_format($r[COL_INTEREST]),
        $r[COL_REMARKS]
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $queryNoLimit->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }
}
