<?php
class User extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }
  }

  public function mentor() {
    $data['title'] = "Pengguna";
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
    $this->db->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"inner");
    $this->db->where(TBL__USERS.".".COL_ROLEID, ROLEPSIKOLOG);
    $this->db->order_by(TBL__USERS.".".COL_USERNAME, 'asc');
    $data['res'] = $this->db->get(TBL__USERS)->result_array();
    $data['role'] = ROLEPSIKOLOG;
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/user/index', $data);
  }

  public function member() {
    $data['title'] = "Pengguna";
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
    $this->db->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"inner");
    $this->db->where_in(TBL__USERS.".".COL_ROLEID, array(ROLEMEMBER_REG, ROLEMEMBER_GROUP, ROLEMEMBER_COY));
    $this->db->order_by(TBL__USERS.".".COL_USERNAME, 'asc');
    $data['res'] = $this->db->get(TBL__USERS)->result_array();
    $data['role'] = '';
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/user/index', $data);
  }

  public function add($role='') {
    $data['title'] = "Form Pengguna";
    $data['role'] = $role;

    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => COL_EMAIL,
          'label' => COL_EMAIL,
          'rules' => 'required|valid_email|is_unique[_userinformation.Email]',
          'errors' => array('is_unique' => 'Email sudah digunakan.')
        ),
        array(
          'field' => COL_PASSWORD,
          'label' => COL_PASSWORD,
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[Password]',
          'errors' => array('matches' => 'Kolom Ulangi Password wajib sama dengan Password.')
        )
      ));

      if(!$this->form_validation->run()) {
        $err = validation_errors();
        ShowJsonError($err);
        return false;
      }

      if(empty($role)) {
        $role = $this->input->post(COL_ROLEID);
      }
      $email = $this->input->post(COL_EMAIL);
      $type = $this->input->post(COL_KD_TYPE);
      $verify = $this->input->post("Verify");
      $scheduleFixed = $this->input->post("ScheduleFixed");
      $scheduleCalendar = $this->input->post("ScheduleCalendar");

      $userdata = array(
        COL_USERNAME => $email,
        COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
        COL_ROLEID => $role
      );
      $userinfo = array(
        COL_USERNAME => $email,
        COL_EMAIL => $email,
        COL_NM_FIRSTNAME => $this->input->post(COL_NM_FIRSTNAME),
        COL_NM_LASTNAME => $this->input->post(COL_NM_LASTNAME),
        COL_NM_NICKNAME => $this->input->post(COL_NM_NICKNAME),
        COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
        COL_NM_BIO => $this->input->post(COL_NM_BIO),
        COL_NM_PROVINCE => $this->input->post(COL_NM_PROVINCE),
        COL_NM_CITY => $this->input->post(COL_NM_CITY),
        COL_DATE_BIRTH => $this->input->post(COL_DATE_BIRTH),
        COL_NO_PHONE => $this->input->post(COL_NO_PHONE),
        COL_NM_MARITALSTATUS => $this->input->post(COL_NM_MARITALSTATUS),
        COL_NM_OCCUPATION => $this->input->post(COL_NM_OCCUPATION),
        COL_NUM_EXPERIENCE => !empty($this->input->post(COL_NUM_EXPERIENCE)) ? toNum($this->input->post(COL_NUM_EXPERIENCE)) : 0,
        COL_NM_EDUCATIONALBACKGROUND => !empty($this->input->post(COL_NM_EDUCATIONALBACKGROUND)) ? urldecode($this->input->post(COL_NM_EDUCATIONALBACKGROUND)) : null,
        COL_DATE_REGISTERED => date('Y-m-d')
      );

      $arrType = array();
      $arrScheduleFixed = array();
      $arrScheduleCalendar = array();

      if(!empty($type)) {
        foreach($type as $s) {
          $arrType[] = array(COL_USERNAME=>$email, COL_KD_TYPE=>$s);
        }
      }
      if(!empty($scheduleFixed)) {
        $scheduleFixed = json_decode(urldecode($scheduleFixed));
        foreach($scheduleFixed as $s) {
          $arrScheduleFixed[] = array(
            COL_USERNAME=>$email,
            COL_KD_SCHEDULETYPE=>'FIXED',
            COL_KD_SCHEDULEDAY=>$s->Day,
            COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
            COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
          );
        }
      }
      if(!empty($scheduleCalendar)) {
        $scheduleCalendar = json_decode(urldecode($scheduleCalendar));
        foreach($scheduleCalendar as $s) {
          $arrScheduleCalendar[] = array(
            COL_USERNAME=>$email,
            COL_KD_SCHEDULETYPE=>'CALENDAR',
            COL_KD_SCHEDULEDAY=>$s->Day,
            COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
            COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
          );
        }
      }

      $res = true;
      $this->db->trans_begin();
      try {
        if(!empty($arrType)) {
          $res = $this->db->insert_batch(TBL_USERTYPE, $arrType);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }
        if(!empty($arrScheduleFixed)) {
          $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleFixed);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }
        if(!empty($arrScheduleCalendar)) {
          $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleCalendar);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }

        $res = $this->db->insert(TBL__USERS, $userdata);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert(TBL__USERINFORMATION, $userinfo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $link = site_url('site/home/verify/'.GetEncryption($email));
        $mailSubj = 'MentorHuis - Verifikasi Akun';
        $mailContent = @"
        <p>
          Terima kasih sudah bergabung di <strong>MentorHuis</strong>, silakan klik link dibawah untuk mengaktifkan akun anda:
        </p>
        $link
        ";
        $send = SendMail($email, $mailSubj, $mailContent);

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/user/'.($role==ROLEPSIKOLOG?'mentor':'member'))));
        return;
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }

    $data['rScheduleFixed'] = array();
    $data['rScheduleCalendar'] = array();
    $data['rUserBills'] = array();
    $this->template->load('main', 'admin/user/form', $data);
  }

  public function edit($id) {
    $id = GetDecryption($id);
    $rdata = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner")
    ->where(TBL__USERS.".".COL_USERNAME, $id)
    ->get(TBL__USERS)->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    $data['title'] = "Form Pengguna";
    $data['edit'] = true;
    $data['role'] = $rdata[COL_ROLEID];
    $data['data'] = $rdata;

    if(!empty($_POST)) {
      $type = $this->input->post(COL_KD_TYPE);
      $verify = $this->input->post("Verify");
      $scheduleFixed = $this->input->post("ScheduleFixed");
      $scheduleCalendar = $this->input->post("ScheduleCalendar");
      $userinfo = array(
        COL_NM_FIRSTNAME => $this->input->post(COL_NM_FIRSTNAME),
        COL_NM_LASTNAME => $this->input->post(COL_NM_LASTNAME),
        COL_NM_NICKNAME => $this->input->post(COL_NM_NICKNAME),
        COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
        COL_NM_BIO => $this->input->post(COL_NM_BIO),
        COL_NM_PROVINCE => $this->input->post(COL_NM_PROVINCE),
        COL_NM_CITY => $this->input->post(COL_NM_CITY),
        COL_DATE_BIRTH => $this->input->post(COL_DATE_BIRTH),
        COL_NO_PHONE => $this->input->post(COL_NO_PHONE),
        COL_NM_MARITALSTATUS => $this->input->post(COL_NM_MARITALSTATUS),
        COL_NM_OCCUPATION => $this->input->post(COL_NM_OCCUPATION),
        COL_NUM_EXPERIENCE => !empty($this->input->post(COL_NUM_EXPERIENCE)) ? toNum($this->input->post(COL_NUM_EXPERIENCE)) : 0,
        COL_NM_EDUCATIONALBACKGROUND => !empty($this->input->post(COL_NM_EDUCATIONALBACKGROUND)) ? urldecode($this->input->post(COL_NM_EDUCATIONALBACKGROUND)) : null ,
      );

      $arrType = array();
      $arrScheduleFixed = array();
      $arrScheduleCalendar = array();

      if(!empty($type)) {
        foreach($type as $s) {
          $arrType[] = array(COL_USERNAME=>$id, COL_KD_TYPE=>$s);
        }
      }
      if(!empty($scheduleFixed)) {
        $scheduleFixed = json_decode(urldecode($scheduleFixed));
        foreach($scheduleFixed as $s) {
          $arrScheduleFixed[] = array(
            COL_USERNAME=>$id,
            COL_KD_SCHEDULETYPE=>'FIXED',
            COL_KD_SCHEDULEDAY=>$s->Day,
            COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
            COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
          );
        }
      }
      if(!empty($scheduleCalendar)) {
        $scheduleCalendar = json_decode(urldecode($scheduleCalendar));
        foreach($scheduleCalendar as $s) {
          $arrScheduleCalendar[] = array(
            COL_USERNAME=>$id,
            COL_KD_SCHEDULETYPE=>'CALENDAR',
            COL_KD_SCHEDULEDAY=>$s->Day,
            COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
            COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
          );
        }
      }

      $res = true;
      $this->db->trans_begin();
      try {
        $this->db->where(COL_USERNAME, $id)->delete(TBL_USERTYPE);
        $this->db->where(COL_USERNAME, $id)->delete(TBL_USERSCHEDULE);

        if(!empty($arrType)) {
          $res = $this->db->insert_batch(TBL_USERTYPE, $arrType);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }
        if(!empty($arrScheduleFixed)) {
          $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleFixed);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }
        if(!empty($arrScheduleCalendar)) {
          $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleCalendar);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }

        $res = $this->db->where(COL_USERNAME, $id)->update(TBL__USERINFORMATION, $userinfo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if(!empty($verify)) {
          $res = $this->db->where(COL_USERNAME, $id)->update(TBL__USERS, array(COL_ISSUSPEND=>FALSE));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $link = site_url('site/home/verify/'.GetEncryption($rdata[COL_EMAIL]));
          $mailSubj = 'MentorHuis - Verifikasi Akun';
          $mailContent = @"
          <p>
            Terima kasih sudah bergabung di <strong>MentorHuis</strong>, silakan klik link dibawah untuk mengaktifkan akun anda:
          </p>
          $link
          ";
          $send = SendMail($rdata[COL_EMAIL], $mailSubj, $mailContent);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/user/'.($rdata[COL_ROLEID]==ROLEPSIKOLOG?'mentor':'member'))));
        return;
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }

    $arrScheduleFixed = array();
    $rScheduleFixed = $this->db
    ->join(TBL_MDAYS,TBL_MDAYS.'.'.COL_KD_DAY." = ".TBL_USERSCHEDULE.".".COL_KD_SCHEDULEDAY,"left")
    ->where(COL_USERNAME, $rdata[COL_USERNAME])
    ->where(COL_KD_SCHEDULETYPE, 'FIXED')
    ->get(TBL_USERSCHEDULE)
    ->result_array();
    foreach($rScheduleFixed as $s) {
      $arrScheduleFixed[] = array(
        'Day' => $s[COL_KD_SCHEDULEDAY],
        'DayText' => $s[COL_NM_DAY],
        'TimeFrom' => $s[COL_DATE_SCHEDULETIME_FROM],
        'TimeTo' => $s[COL_DATE_SCHEDULETIME_TO]
      );
    }

    $arrScheduleCalendar = array();
    $rScheduleCalendar = $this->db
    ->where(COL_USERNAME, $rdata[COL_USERNAME])
    ->where(COL_KD_SCHEDULETYPE, 'CALENDAR')
    ->where(COL_DATE_SCHEDULEDATE.' >= ', date('Y-m-d'))
    ->get(TBL_USERSCHEDULE)
    ->result_array();
    foreach($rScheduleCalendar as $s) {
      $arrScheduleCalendar[] = array(
        'Day' => $s[COL_DATE_SCHEDULEDATE],
        'DayText' => $s[COL_DATE_SCHEDULEDATE],
        'TimeFrom' => $s[COL_DATE_SCHEDULETIME_FROM],
        'TimeTo' => $s[COL_DATE_SCHEDULETIME_TO]
      );
    }

    $arrBills = array();
    $rUserBills = $this->db
    ->where(COL_USERNAME, $rdata[COL_USERNAME])
    ->get(TBL_USERBILL)
    ->result_array();
    foreach($rUserBills as $s) {
      $arrBills[] = array(
        'Bank' => $s[COL_KD_BANK],
        'AccountNo' => $s[COL_NM_ACCOUNTNO],
        'AccountName' => $s[COL_NM_ACCOUNTNAME]
      );
    }

    $data['rScheduleFixed'] = $rScheduleFixed;
    $data['rScheduleCalendar'] = $rScheduleCalendar;
    $data['rUserBills'] = $rUserBills;
    $data['data']['ScheduleFixed'] = json_encode($arrScheduleFixed);
    $data['data']['ScheduleCalendar'] = json_encode($arrScheduleCalendar);
    $data['data']['UserBills'] = json_encode($arrBills);
    $this->template->load('main', 'admin/user/form', $data);
  }

  public function delete() {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $res = $this->db->where(COL_USERNAME, $datum)->delete(TBL__USERS);
      if($res) {
        $deleted++;
      }
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada data dihapus");
    }
  }

  public function activate($opt = 0) {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      if($opt == 0 || $opt == 1) {
        if($this->db->where(COL_USERNAME, $datum)->update(TBL__USERS, array(COL_ISSUSPEND=>$opt))) {
          $deleted++;
        }
        if($opt == 0) {
          $this->db->where(COL_USERNAME, $datum)->update(TBL__USERINFORMATION, array(COL_DATE_REGISTERED=>date('Y-m-d H:i:s')));
        }
      }
      else {
        if($this->db->where(COL_USERNAME, $datum)->update(TBL__USERS, array(COL_PASSWORD=>MD5('123456')))) {
          $deleted++;
        }
      }
    }
    if($deleted){
      ShowJsonSuccess($deleted." data diubah");
    }else{
      ShowJsonError("Tidak ada data yang diubah");
    }
  }
}
?>
