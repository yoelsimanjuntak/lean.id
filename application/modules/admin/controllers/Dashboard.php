<?php
class Dashboard extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(IsLogin()) {
      $ruser = GetLoggedUser();
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        redirect('site/home');
      }
    }
  }

  public function index() {
    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }

    $data['title'] = "Dashboard";
    $this->template->load('main', 'admin/dashboard/index', $data);
  }

  public function member_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_DATE_REGISTERED=>'desc');
    $orderables = array(null,COL_NM_FULLNAME,COL_NM_IDENTITYNO,COL_USERNAME,COL_DATE_REGISTERED);
    $cols = array(COL_NM_FULLNAME,COL_NM_IDENTITYNO,COL_NM_PHONENO,COL_USERNAME,COL_DATE_REGISTERED);

    $queryAll = $this->db
    ->join(TBL__USERS.' u','u.'.COL_USERNAME." = ".TBL__USERINFORMATION.".".COL_USERNAME,"inner")
    ->where('u.'.COL_ROLEID, ROLEMEMBER)
    ->get(TBL__USERINFORMATION);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_USERNAME) $item = TBL__USERINFORMATION.'.'.COL_USERNAME;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->join(TBL__USERS.' u','u.'.COL_USERNAME." = ".TBL__USERINFORMATION.".".COL_USERNAME,"inner")
    ->where('u.'.COL_ROLEID, ROLEMEMBER)
    ->get_compiled_select(TBL__USERINFORMATION, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/dashboard/member-delete/'.GetEncryption($r[COL_USERNAME])).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/dashboard/member-edit/'.GetEncryption($r[COL_USERNAME])).'" class="btn btn-xs btn-outline-success btn-popup-action"><i class="fas fa-pencil"></i></a>',
        $r[COL_NM_FULLNAME],
        $r[COL_NM_IDENTITYNO],
        $r[COL_NM_PHONENO],
        $r[COL_USERNAME],
        date('Y-m-d', strtotime($r[COL_DATE_REGISTERED])),
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function member_add() {
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => COL_NM_EMAIL,
          'label' => COL_NM_EMAIL,
          'rules' => 'required|valid_email|is_unique[_userinformation.Nm_Email]',
          'errors' => array('is_unique' => 'Email sudah digunakan.')
        ),
        array(
          'field' => COL_PASSWORD,
          'label' => COL_PASSWORD,
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        )
      ));

      if(!$this->form_validation->run()) {
        $err = validation_errors();
        ShowJsonError($err);
        return false;
      }

      $userdata = array(
        COL_USERNAME => $this->input->post(COL_NM_EMAIL),
        COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
        COL_ROLEID => ROLEMEMBER
      );
      $userinfo = array(
        COL_USERNAME => $this->input->post(COL_NM_EMAIL),
        COL_NM_EMAIL => $this->input->post(COL_NM_EMAIL),
        COL_NM_FULLNAME => $this->input->post(COL_NM_FULLNAME),
        COL_NM_IDENTITYNO => $this->input->post(COL_NM_IDENTITYNO),
        COL_NM_GENDER => $this->input->post(COL_NM_GENDER),
        COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
        COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO),
        COL_DATE_REGISTERED => date('Y-m-d')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL__USERS, $userdata);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert(TBL__USERINFORMATION, $userinfo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    } else {
      $data = array();
      $this->load->view('admin/dashboard/_member', $data);
    }
  }

  public function member_edit($username) {
    $username = GetDecryption($username);
    if(!empty($_POST)) {
      $userinfo = array(
        COL_NM_FULLNAME => $this->input->post(COL_NM_FULLNAME),
        COL_NM_IDENTITYNO => $this->input->post(COL_NM_IDENTITYNO),
        COL_NM_GENDER => $this->input->post(COL_NM_GENDER),
        COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
        COL_NM_PHONENO => $this->input->post(COL_NM_PHONENO)
      );

      $res = $this->db->where(COL_USERNAME, $username)->update(TBL__USERINFORMATION, $userinfo);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        return;
      }

      ShowJsonSuccess('OK');
      return;
    } else {
      $data['data'] = $rdata = $this->db->where(COL_USERNAME, $username)->get(TBL__USERINFORMATION)->row_array();
      if(empty($rdata)) {
        echo 'Data tidak valid.';
        return;
      }
      $this->load->view('admin/dashboard/_member', $data);
    }
  }

  public function member_delete($username) {
    $username = GetDecryption($username);

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_USERNAME, $username)->delete(TBL__USERS);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db->where(COL_USERNAME, $username)->delete(TBL__USERINFORMATION);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
  }
}
