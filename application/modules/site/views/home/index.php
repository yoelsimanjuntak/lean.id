<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FULLNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_IMAGELOCATION] ? MY_UPLOADURL.$ruser[COL_NM_IMAGELOCATION] : MY_IMAGEURL.'user.jpg';
}
?>
<style>
.list-group-item:last-child {
  border-bottom: none;
}
</style>
<div class="content-wrapper">
  <section class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
            <div class="card card-purple card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="<?=$displaypicture?>" alt="<?=$displayname?>">
                </div>

                <h3 class="profile-username text-center"><?=$displayname?></h3>
                <p class="text-muted text-center">Terdaftar sejak <?=date('d-m-Y', strtotime($ruser[COL_DATE_REGISTERED]))?></p>

                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right"><?=$ruser[COL_NM_EMAIL]?></a>
                  </li>
                  <li class="list-group-item">
                    <b>NIK</b> <a class="float-right"><?=$ruser[COL_NM_IDENTITYNO]?></a>
                  </li>
                  <li class="list-group-item">
                    <b>No. Telp</b> <a class="float-right"><?=$ruser[COL_NM_PHONENO]?></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="row">
              <div class="col-sm-12">
                <div class="card card-outline card-info">
                  <div class="card-header">
                    <h5 class="card-title font-weight-light">Pinjaman Aktif</h5>
                  </div>
                  <div class="card-body p-0">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>No. Pinjaman</th>
                          <th>Jumlah</th>
                          <th>Bunga (%)</th>
                          <th>Jumlah Cicilan</th>
                          <th>Total Cicilan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $rloanaktif = $this->db
                        ->select("*, (select count(*) from tloandetail det where det.IdLoan = tloan.IdLoan) as JlhCicilan, (select sum(det.Amount) from tloandetail det where det.IdLoan = tloan.IdLoan) as TotalCicilan, , (select sum(det.Interest) from tloandetail det where det.IdLoan = tloan.IdLoan) as TotalBunga")
                        ->where(array(COL_USERNAME=>$ruser[COL_USERNAME],COL_LOANSTATUS=>'AKTIF'))
                        ->get(TBL_TLOAN)
                        ->result_array();
                        if(count($rloanaktif) > 0) {
                          foreach($rloanaktif as $l) {
                            ?>
                            <tr>
                              <td><?="LN-".str_pad($l[COL_IDLOAN], 5, "0", STR_PAD_LEFT)?></td>
                              <td class="text-right"><?=number_format($l[COL_LOANAMOUNT])?></td>
                              <td class="text-right"><?=number_format($l[COL_LOANINTEREST], 2)?></td>
                              <td class="text-right"><?=number_format($l["JlhCicilan"])?></td>
                              <td class="text-right"><?=number_format($l["TotalCicilan"])?></td>
                            </tr>
                            <?php
                          }
                        } else {
                          echo '<tr><td colspan="5" class="text-center font-italic">Tidak ada data.</td></tr>';
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="card card-outline card-secondary">
                  <div class="card-header">
                    <h5 class="card-title font-weight-light">PERMOHONAN</h5>
                  </div>
                  <div class="card-body p-0">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>No. Pinjaman</th>
                          <th>Jumlah</th>
                          <th>Bunga (%)</th>
                          <th>Tgl. Pengajuan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $rloanpending = $this->db
                        ->where(array(COL_USERNAME=>$ruser[COL_USERNAME],COL_LOANSTATUS=>'PERMOHONAN'))
                        ->get(TBL_TLOAN)
                        ->result_array();
                        if(count($rloanpending) > 0) {
                          foreach($rloanpending as $l) {
                            ?>
                            <tr>
                              <td><?="LN-".str_pad($l[COL_IDLOAN], 5, "0", STR_PAD_LEFT)?></td>
                              <td class="text-right"><?=number_format($l[COL_LOANAMOUNT])?></td>
                              <td class="text-right"><?=number_format($l[COL_LOANINTEREST], 2)?></td>
                              <td class="text-center"><?=date('d-m-Y', strtotime($l[COL_CREATEDON]))?></td>
                            </tr>
                            <?php
                          }
                        } else {
                          echo '<tr><td colspan="5" class="text-center font-italic">Tidak ada data.</td></tr>';
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>
</div>
