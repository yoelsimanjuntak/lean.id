<?php
class Template {
		var $template_data = array();

		function set($content_area, $value)
		{
			$this->template_data[$content_area] = $value;
		}

		function load($template = '', $view = '' , $view_data = array(), $return = FALSE, $is_plain = FALSE, $bodyclass = 'layout-top-nav layout-navbar-fixed')
		{
			$this->CI =& get_instance();

			$this->set('content' , $this->CI->load->view($view, $view_data, TRUE));
			$this->set('is_plain' , $is_plain);
			$this->set('bodyclass' , $bodyclass);
			$this->CI->load->view('_layouts/'.$template, $this->template_data);
		}
}
 ?>
