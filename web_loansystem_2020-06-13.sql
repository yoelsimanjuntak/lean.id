# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: web_loansystem
# Generation Time: 2020-06-13 11:40:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Anggota');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','PINJAMAN.ID'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Pinjaman Online'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','-'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `NM_Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FullName` varchar(50) DEFAULT NULL,
  `NM_IdentityNo` varchar(50) DEFAULT NULL,
  `DATE_Birth` date DEFAULT NULL,
  `NM_Gender` varchar(50) DEFAULT NULL,
  `NM_Address` text,
  `NM_Provinsi` varchar(200) DEFAULT NULL,
  `NM_Kota` varchar(200) DEFAULT NULL,
  `NM_PhoneNo` varchar(50) DEFAULT NULL,
  `NM_ImageLocation` varchar(250) DEFAULT NULL,
  `NM_About` text,
  `DATE_Registered` date DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  UNIQUE KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `NM_Email`, `NM_FullName`, `NM_IdentityNo`, `DATE_Birth`, `NM_Gender`, `NM_Address`, `NM_Provinsi`, `NM_Kota`, `NM_PhoneNo`, `NM_ImageLocation`, `NM_About`, `DATE_Registered`)
VALUES
	(1,'admin','yoelrolas@gmail.com','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,'085359867032',NULL,NULL,'2018-08-17'),
	(4,'budi@gmail.com','budi@gmail.com','Budi','1271031208950002',NULL,'LAKI-LAKI','Alamat',NULL,NULL,'081211221212',NULL,NULL,'2020-06-12'),
	(5,'jesika@gmail.com','jesika@gmail.com','Jesika','1271031208960002',NULL,'PEREMPUAN','Alamat',NULL,NULL,'081311331313',NULL,NULL,'2020-06-12');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2020-06-13 17:18:54','::1'),
	('budi@gmail.com','bbfb3b97637d3caa18d4f73c6bf1b3b6',2,0,'2020-06-13 16:38:26','::1'),
	('jesika@gmail.com','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL);

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tloan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tloan`;

CREATE TABLE `tloan` (
  `IdLoan` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `LoanStatus` varchar(10) DEFAULT '',
  `LoanAmount` double NOT NULL,
  `LoanInterest` double NOT NULL,
  `LoanTerm` double NOT NULL,
  `Remarks` text,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`IdLoan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tloan` WRITE;
/*!40000 ALTER TABLE `tloan` DISABLE KEYS */;

INSERT INTO `tloan` (`IdLoan`, `UserName`, `LoanStatus`, `LoanAmount`, `LoanInterest`, `LoanTerm`, `Remarks`, `CreatedOn`, `CreatedBy`)
VALUES
	(1,'budi@gmail.com','AKTIF',30000000,0.79,36,'TEST','2020-06-13 00:12:57','admin'),
	(4,'jesika@gmail.com','AKTIF',50000000,0.79,48,'TESTING','2020-06-13 00:22:53','admin'),
	(5,'jesika@gmail.com','LUNAS',10000000,0.79,2,'Pinjaman Rp. 10jt','2020-06-13 01:38:03','admin'),
	(6,'budi@gmail.com','DITOLAK',25000000,0.79,24,'TEST','2020-06-13 16:07:05','admin');

/*!40000 ALTER TABLE `tloan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tloandetail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tloandetail`;

CREATE TABLE `tloandetail` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdLoan` bigint(10) unsigned NOT NULL,
  `DateDue` date NOT NULL,
  `DatePaid` date NOT NULL,
  `Amount` double NOT NULL,
  `Interest` double NOT NULL,
  `Remarks` text,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`),
  KEY `FK_LoanDetail_Loan` (`IdLoan`),
  CONSTRAINT `FK_LoanDetail_Loan` FOREIGN KEY (`IdLoan`) REFERENCES `tloan` (`IdLoan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tloandetail` WRITE;
/*!40000 ALTER TABLE `tloandetail` DISABLE KEYS */;

INSERT INTO `tloandetail` (`Uniq`, `IdLoan`, `DateDue`, `DatePaid`, `Amount`, `Interest`, `Remarks`, `CreatedOn`, `CreatedBy`)
VALUES
	(7,5,'0000-00-00','2020-06-13',5000000,79000,'Cicilan ke-1','2020-06-13 01:46:45','admin'),
	(8,5,'0000-00-00','2020-07-13',5000000,79000,'Cicilan ke-2','2020-06-13 01:46:55','admin');

/*!40000 ALTER TABLE `tloandetail` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
